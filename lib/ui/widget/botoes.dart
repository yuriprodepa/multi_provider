import 'package:flutter/material.dart';
import 'package:multi_provider/core/view_models/first_view_model.dart';
import 'package:provider/provider.dart';


Widget buildBotoes(BuildContext context) {
  final firstViewModel = Provider.of<FirstViewModel>(context);
  return Positioned(
    bottom: 20,
    child: Container(
      padding: const EdgeInsets.all(16.0),
      height: 70,
      width: 350,
      alignment:
          FractionalOffset.bottomLeft, // Alinha embaixo da tela na esquerda
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RaisedButton(
            child: Text("Maracuja"),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: (() {
              firstViewModel.setName("Maracuja");
            }),
            color: Colors.black,
            textColor: Colors.yellow,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            splashColor: Colors.grey,
          ),
          RaisedButton(
            child: Text("Limonada"),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: (() {
              firstViewModel.setName("Limonada");
            }),
            color: Colors.black,
            textColor: Colors.yellow,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            splashColor: Colors.grey,
          ),
          RaisedButton(
            child: Text("Vazio"),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: (() {
              firstViewModel.setName("Vazio");
            }),
            color: Colors.black,
            textColor: Colors.yellow,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            splashColor: Colors.grey,
          )
        ],
      ),
    ),
  );
}
