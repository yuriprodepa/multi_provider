import 'package:flutter/material.dart';

Widget getCircularProgress(String msn) {
  return Positioned(
      top: 200,
      child: Container(
        padding: const EdgeInsets.all(16.0),
        height: 70,
        width: 350,
        alignment: Alignment.center,
        child: Row(
          children: <Widget>[
            Text("$msn: "),
            CircularProgressIndicator(),
          ]
        ),
      ),
    ); 
}