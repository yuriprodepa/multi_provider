import 'package:flutter/material.dart';
import 'package:multi_provider/core/enums/view_state.dart';
import 'package:multi_provider/core/view_models/first_view_model.dart';
import 'package:multi_provider/ui/widget/shared/circular_progress.dart';
import 'package:provider/provider.dart';

//Carrega de forma assincrona
Widget buildTextoNomeStream(BuildContext context) {
  final firstViewModel = Provider.of<FirstViewModel>(context);
  return Positioned(
      top: 200,
      child: Container(
        padding: const EdgeInsets.all(16.0),
        height: 100,
        width: 350,
        alignment: Alignment.center,
        child: StreamBuilder<String>(
            stream: firstViewModel.nomeStream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) return CircularProgressIndicator();
              return Text(
                snapshot.data,
                style: Theme.of(context).textTheme.headline,
              );
            }),
      ));
}

//Carrega de forma sincrona
Widget buildTextoNome(BuildContext context) {
  final firstViewModel = Provider.of<FirstViewModel>(context);
  if (firstViewModel.state == ViewState.BUSY) {
    return Positioned(
      top: 300,
      child: Container(
          padding: const EdgeInsets.all(16.0),
          height: 100,
          width: 350,
          alignment: Alignment.center,
          child: getCircularProgress("Carregando nome")),
    );
  }
  if (firstViewModel.state == ViewState.RESPONSE_ERROR) {
    return Positioned(
      top: 300,
      child: Container(
          padding: const EdgeInsets.all(16.0),
          height: 100,
          width: 350,
          alignment: Alignment.center,
          child: Container()
      ),
    );
  } else
    return Positioned(
        top: 300,
        child: Container(
          padding: const EdgeInsets.all(16.0),
          height: 100,
          width: 350,
          alignment: Alignment.center,
          child: Text(
            firstViewModel.nome,
            style: Theme.of(context).textTheme.headline,
          ),
        ));
}
