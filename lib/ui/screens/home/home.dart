import 'package:flutter/material.dart';
import 'package:multi_provider/core/view_models/first_view_model.dart';
import 'package:multi_provider/ui/widget/botoes.dart';
import 'package:multi_provider/ui/widget/texto_nome.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final firstViewModel = Provider.of<FirstViewModel>(context);

    return Scaffold(
      key: firstViewModel.scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          buildTextoNomeStream(context),
          buildTextoNome(context),
          buildBotoes(context),
        ],
      ),
    );
  }
}
