import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_provider/core/view_models/first_view_model.dart';
import 'package:multi_provider/ui/screens/home/home.dart';
import 'package:multi_provider/ui/values/routes.dart';
import 'package:multi_provider/ui/values/strings.dart';
import 'package:provider/provider.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<FirstViewModel>(
          builder: (context) => FirstViewModel(),
        ),
      ],
      child: MaterialApp(
        title: APP_NAME,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'Montserrat',
          textTheme: TextTheme(
            headline: TextStyle(
                fontSize: 50.0,
                fontWeight: FontWeight.bold,
                color: Colors.black),
          ),
        ),
        routes: {
          HOME_ROUTE: (context) => HomeScreen(),
        },
        initialRoute: HOME_ROUTE,
      ),
    );
  }
}
