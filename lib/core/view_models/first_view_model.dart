import 'dart:async';

import 'package:multi_provider/core/enums/view_state.dart';
import 'package:multi_provider/core/view_models/base_view_model.dart';
import 'package:flutter/material.dart';

class FirstViewModel extends BaseViewModel {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  FirstViewModel();

  //Tubo de comunicação
  StreamController<String> _streamController =  StreamController<String>();
  Stream<String> get nomeStream => _streamController.stream;

  String _nome = "";
  String get nome => _nome;

  void setName(String nome){
    setState(ViewState.BUSY); // Ocupado
    if(nome != "Vazio"){
      _streamController.sink.add(nome); // Recupera por stream
      _nome = nome; //Recuper sem stream
      setState(ViewState.IDLE); // Liberado
    }else{
      _streamController.sink.add(null);
      setState(ViewState.RESPONSE_ERROR);
    }
    notifyListeners();
  }

    
  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

}